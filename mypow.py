
# "Flop is floating operation. Hashing is integer operation"
# Have not linked blocks together
# could do class implementation but i imagine it's slower

from hashlib import sha256, sha1
from uuid import uuid4
import numpy as np
import json, time
import matplotlib.pyplot as plt

def hashBlock(block):
	block_serialization = json.dumps(block, sort_keys=True).encode('utf-8')
	block_hash = sha256(block_serialization).hexdigest()

	return block_hash

# actually alters the old block; is probably bad but may be desired
def mineNextBlock(old_block):
	bits = old_block['bits']
	difficulty = 2 ** bits
	max_nonce = 2 ** 32
	target = 2 ** (256 - bits)	# we're trying to find a number that converts from hexa>int lower than this
	old_block['nonce'] = 0

	for nonce in range(max_nonce):
		hashy = hashBlock(old_block)

		if int(hashy, 16) < target:
			return {
				'prev_hash': hashy, 
				'transactions': str(uuid4()), 
				'bits': bits, 
				'nonce': 0,
			}

		old_block['nonce'] = old_block['nonce'] + 1

	print("Failed after %d (max_nonce) tries\n" % max_nonce)
	return {
		'prev_hash': 'FAILED', 
		'transactions': str(uuid4()), 
		'bits': bits, 
		'nonce': max_nonce,
	}

# creates a new block and attempts to find the next block for N trials
def testTime(N=1000, bits=8):

	times = []

	print('Testing with N=%d...' % N)
	start_test_time = time.time()
	for trial in range(N):
		block = {
			'prev_hash': None, 
			'transactions': str(uuid4()), 
			'bits': bits, 
			'nonce': 0,
		}

		start_trial_time = time.time()
		mineNextBlock(block)
		end_trial_time = time.time()

		trial_time = end_trial_time-start_trial_time
		times.append(trial_time)
	end_test_time = time.time()

	total_time = end_test_time-start_test_time

	print('Test finished. Results:')
	print('\tAverage Time (s):', np.mean(times))
	print('\tVariance (s):', np.var(times))
	print('\tTotal Test Time (s):', total_time)

	plt.plot(list(range(len(times))), times)
	plt.title('Time for Each Mine')
	plt.xlabel('Trial (N)')
	plt.ylabel('Time (s)')
	plt.show()

	return 'Test Successful'

def testOneMin():
	pass

if __name__ == '__main__':

	### test time to mine blocks ###
	input_n = int(input('What sample size, N={int, n>0}? '))
	in_bits = int(input('What difficulty in bits, bits={int, 0<bits<33}? '))
	testTime(input_n, in_bits)

	### mining for next block and verifying ###
	'''
	genesis_block = {
		'prev_hash': None,
		'transactions': 123, 	# will just be some random uuid until later (transactions to be added)
		'bits': 8,
		'nonce': 0,
		}

	second_block = mineNextBlock(genesis_block)
	third_block = mineNextBlock(second_block)

	print('Genesis:\n', genesis_block, '\n', hashBlock(genesis_block), '\n\n')
	print('Second:\n', second_block, '\n', hashBlock(second_block), '\n\n')
	print('Third:\n', third_block, '\n', hashBlock(third_block), '\n\n')
	'''






